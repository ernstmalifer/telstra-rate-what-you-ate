Telstra Rate What You Ate
========================

1) Installing
----------------------------------

Requirements:

-   A webserver with [PHP][1] module
-   [MySQL][2] for database
-   [Composer][3] for package manager
-   [Node][4] and [NPM][5]
-   [Bower][6]
-   [Grunt][7] Taskrunner - concatenates and minifies angular scripts

Optional for stylesheets

-   [Ruby][8] for compass/sass (`compass watch` the web directory)

### Use Composer (*recommended*)

As Symfony uses [Composer][3] to manage its dependencies, the recommended way
to create a new project is to use it.

    composer install

Composer will handle the bower components update

2) Create DB and Update DB Config
----------------------------------

Create a database, composer will inquire about the database configuration and the mailer


3) Run database migration
----------------------------------

    php app/console doctrine:migrations:migrate


4) Run grunt watch
----------------------------------

    grunt watch


5) Access the app /web with a brower
----------------------------------


[1]:  http://php.net/
[2]:  http://mysql.com/
[3]:  http://getcomposer.org/
[4]:  http://nodejs.org/
[5]:  http://npmjs.org/
[6]:  http://bower.io/
[7]:  http://gruntjs.com/
[8]:  https://ruby-lang.org/