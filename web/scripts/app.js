var ratewhatyouateApp = angular.module('ratewhatyouateApp',['ngRoute', 'ngResource', 'restangular', 'ui.bootstrap', 'angularFileUpload']);

ratewhatyouateApp.config(function($routeProvider, RestangularProvider, RestangularProvider){

	RestangularProvider.setBaseUrl(apiurl);

	RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
		var extractedData;

		if (operation === "getList") {
        	extractedData = data.data;
      	} else {
        	extractedData = data.data;
      	}
      	return extractedData;
    });

	$routeProvider
		.when('/', {
			templateUrl: baseurl + 'scripts/views/home.html',
			controller: 'HomeController'
		})
		.when('/menu', {
			templateUrl: baseurl + 'scripts/views/menu.html',
			controller: 'MenuController'
		})
		.when('/locations', {
			templateUrl: baseurl + 'scripts/views/locations.html',
			controller: 'LocationsController'
		})
		.when('/categories', {
			templateUrl: baseurl + 'scripts/views/categories.html',
			controller: 'CategoriesController'
		});
});

ratewhatyouateApp.run(function($rootScope){
	$rootScope.baseurl = baseurl;
	$rootScope.apiurl = apiurl;
});

ratewhatyouateApp.filter('dateToISO', function() {
  return function(input) {
    input = new Date(input).toISOString();
    return input;
  };
});