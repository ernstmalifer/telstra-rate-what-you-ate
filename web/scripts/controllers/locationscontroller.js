ratewhatyouateApp.controller('LocationsController', function($scope,  LocationsService, $modal, $log, _, $window) {

    $scope.locations = '';
    $scope.hasSelected = false;
    $scope.predicate = 'id';
    $scope.reverse = false;

    $scope.locationservice = LocationsService.getLocations();
    $scope.locationservice.then(function(data) {
        $scope.locations = data;
        _.each($scope.locations, function(locationitem){ locationitem.selected = false; });
    });

    $scope.select = function (all) {
        if(all) {
            _.each($scope.locations, function(locationitem){locationitem.selected = $scope.selectall;});
            $scope.hasSelected = $scope.selectall ?  true : false ;
        } else {
            var selected = _.where($scope.locations, {selected: true});
            $scope.hasSelected = selected.length ?  true : false;
            $scope.selectall = selected.length == $scope.locations.length ? true : false;
        }
    };

    $scope.open = function (size) {
        var addlocationModal = $modal.open({
            templateUrl: baseurl + 'scripts/views/locationsModal.html',
            controller: 'AddLocationModalCtrl',
            size: size
        });
    };

    $scope.edit = function (id) {
        var editlocationModal = $modal.open({
            templateUrl: baseurl + 'scripts/views/locationsModal.html',
            controller: 'EditLocationModalCtrl',
            resolve: {
                id: function() {
                    return angular.copy(id);
                }
            }
        });
    };

    $scope.delete = function (id) {
        var locationsToDelete = _.find($scope.locations, function(locationitem){ return locationitem.id == id; });
        
        if (confirm("Are you sure you want to delete " + locationsToDelete.name + " (id:" + locationsToDelete.id + ")?") == true) {
            LocationsService.deleteLocation(id).then(function(data) {
                $window.location.reload();
            });
        } else {
        }
    };

    $scope.deleteAll = function () {
        var selected = _.where($scope.locations, {selected: true});

        var selectedtext = '\n';
        _.each( selected, 
            function(locationsitemselected){ 
                selectedtext += '   -' + locationsitemselected.name + ' (id:' + locationsitemselected.id + ')\n'; 
            }
        );

        if (confirm('Are you sure you want to delete:' + selectedtext) == true){
            LocationsService.deleteLocation(selected).then(function(data) {
                $window.location.reload();
            });
        }
    };

});

ratewhatyouateApp.controller('AddLocationModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'LocationsService', '$window', function ($scope, $modalInstance, $fileUploader, LocationsService, $window) {

    $scope.new = true;
    $scope.newlocation= {};
    $scope.saveButton = 'Add Location';
    $scope.disableform = false;

    $scope.ok = function(newlocation) {

        if( LocationsService.isLocation(newlocation) ){
            $scope.saveButton = 'Adding...';
            $scope.disableform = true;

            LocationsService.addLocation(newlocation).then(function(data) {
                $modalInstance.close();
                $window.location.reload();
            });
        }
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        return LocationsService.check(form, $scope['newlocation'][form]) ? '' : 'has-error';
    };

}]);

ratewhatyouateApp.controller('EditLocationModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'LocationsService', '$window', 'id', function ($scope, $modalInstance, $fileUploader, LocationsService, $window, id) {

    this.$inject = ['id'];
    $scope.new = false;
    LocationsService.getLocationItem(id).then(function(data) {
        $scope.newlocation = data[0];
    });
    $scope.saveButton = 'Save Location';
    $scope.disableform = false;

    $scope.ok = function(newlocation) {

        if( LocationsService.isLocation(newlocation) ){
            $scope.saveButton = 'Saving...';
            $scope.disableform = true;

            LocationsService.editLocation(id, newlocation).then(function(data) {
                $modalInstance.close();
                $window.location.reload();
            });
        }
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        try { 
            return LocationsService.check(form, $scope['newlocation'][form]) ? '' : 'has-error';
            return MenuService.check(form, $scope['menu'][form]) ? '' : 'has-error';
        } catch(e) {

        }
    };

}]);