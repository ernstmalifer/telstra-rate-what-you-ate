ratewhatyouateApp.controller('MenuController', function($scope, MenuService, $modal, $log, _, $window) {

    $scope.menu = '';
    $scope.menuselect = [];
    $scope.hasSelected = false;
    $scope.predicate = 'id';
    $scope.reverse = false;

    $scope.menuservice = MenuService.getMenu();
    $scope.menuservice.then(function(data) {
        $scope.menu = data;
        _.each($scope.menu, function(menuitem){menuitem.selected = false;});
    });

    $scope.select = function (all) {
        if(all) {
            _.each($scope.menu, function(menuitem){menuitem.selected = $scope.selectall;});
            $scope.hasSelected = $scope.selectall ?  true : false ;
        } else {
            var selected = _.where($scope.menu, {selected: true});
            $scope.hasSelected = selected.length ?  true : false;
            $scope.selectall = selected.length == $scope.menu.length ? true : false;
        }
    };

    $scope.open = function (size) {
	    var addmenuModal = $modal.open({
			templateUrl: baseurl + 'scripts/views/menuModal.html',
			controller: 'AddMenuModalCtrl',
			size: size
		});
	};

    $scope.edit = function (id) {
        var editmenuModal = $modal.open({
            templateUrl: baseurl + 'scripts/views/menuModal.html',
            controller: 'EditMenuModalCtrl',
            resolve: {
                id: function() {
                    return angular.copy(id);
                }
            }
        });
    };

    $scope.delete = function (id) {
        var menuToDelete = _.find($scope.menu, function(menuitem){ return menuitem.id == id; });
        
        if (confirm("Are you sure you want to delete " + menuToDelete.name + " (id:" + menuToDelete.id + ")?") == true) {
            MenuService.deleteMenu(id).then(function(data) {
                $window.location.reload();
            });
        } else {
        }
    };

    $scope.deleteAll = function () {
        var selected = _.where($scope.menu, {selected: true});

        var selectedtext = '\n';
        _.each( selected, 
            function(menuitemselected){ 
                selectedtext += '   -' + menuitemselected.name + ' (id:' + menuitemselected.id + ')\n'; 
            }
        );

        if (confirm('Are you sure you want to delete:' + selectedtext) == true){
            MenuService.deleteMenu(selected).then(function(data) {
                $window.location.reload();
            });
        }
    };
     
});

ratewhatyouateApp.controller('AddMenuModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'MenuService', '$window', function ($scope, $modalInstance, $fileUploader, MenuService, $window) {

    $scope.new = true;
    $scope.menu = {};
    $scope.saveButton = 'Add Menu';
    $scope.disableform = false;

	$scope.ok = function(menu) {

        if( MenuService.isMenu(menu) ){
            $scope.saveButton = 'Adding...';
            $scope.disableform = true;

            MenuService.addMenu(menu).then(function(data) {
                $scope.uploader.queue[0].url = $scope.apiurl + 'api/menu/image/' + data.id;
                $scope.uploader.uploadAll();
            });
        }
	};

	$scope.cancel = function () {
		$modalInstance.close();
	};

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        return MenuService.check(form, $scope['menu'][form]) ? '' : 'has-error';
    };

	$scope.uploader = $fileUploader.create({
        scope: $scope,   
        queueLimit: 1,
    });

    $scope.uploader.filters.push(function(item /*{File|HTMLInput}*/) { // user filter
        return true;
    });

	$scope.uploader.bind('beforeupload', function (event, item) {
    });

    $scope.uploader.bind('progressall', function (event, progress) {
    });

    $scope.uploader.bind('completeall', function (event, items) {
        $modalInstance.close();
        $window.location.reload();
    });

    $scope.controller = {
        isImage: function(item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

}]);

ratewhatyouateApp.controller('EditMenuModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'MenuService', '$window', 'id', function ($scope, $modalInstance, $fileUploader, MenuService, $window, id) {

    this.$inject = ['id'];

    $scope.new = false;
    $scope.item = {};
    MenuService.getMenuItem(id).then(function(data) {
        $scope.menu = data[0];
    });

    $scope.saveButton = 'Save Menu';
    $scope.disableform = false;

    $scope.ok = function(menu) {

        if( MenuService.isMenu(menu) ){
            $scope.addButton = 'Saving...';
            $scope.disableform = true;

            MenuService.editMenu(id, menu).then(function(data) {
                if( $scope.uploader.queue.length ) {
                    $scope.uploader.queue[0].url = $scope.apiurl + 'api/menu/image/' + id;
                    $scope.uploader.uploadAll();
                } else {
                    $modalInstance.close();
                }
            });
        }
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        try { 
            return MenuService.check(form, $scope['menu'][form]) ? '' : 'has-error';
        } catch(e) {

        }
    };

    $scope.clear = function() {
         $scope.uploader.clearQueue();
         $scope.hasItem = false;
    }

    $scope.uploader = $fileUploader.create({
        scope: $scope,   
        queueLimit: 1,
    });

    $scope.uploader.filters.push(function(item /*{File|HTMLInput}*/) { // user filter
        return true;
    });

    $scope.uploader.bind('afteraddingfile', function (event, item) {
        $scope.hasItem = true;
    });

    $scope.uploader.bind('beforeupload', function (event, item) {
    });

    $scope.uploader.bind('progressall', function (event, progress) {
    });

    $scope.uploader.bind('completeall', function (event, items) {
        $modalInstance.close();
        $window.location.reload();
    });

    $scope.controller = {
        isImage: function(item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

}]);