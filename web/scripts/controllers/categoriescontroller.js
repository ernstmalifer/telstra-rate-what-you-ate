ratewhatyouateApp.controller('CategoriesController', function($scope,  CategoriesService, $modal, $log, _, $window) {

    $scope.categories = '';
    $scope.hasSelected = false;
    $scope.predicate = 'id';
    $scope.reverse = false;

    $scope.categoriesservice = CategoriesService.getCategories();
    $scope.categoriesservice.then(function(data) {
        $scope.categories = data;
        _.each($scope.categories, function(categoryitem){ 
            categoryitem.selected = false;
        });
    });

    $scope.select = function (all) {
        if(all) {
            _.each($scope.categories, function(categoryitem){categoryitem.selected = $scope.selectall;});
            $scope.hasSelected = $scope.selectall ?  true : false ;
        } else {
            var selected = _.where($scope.categories, {selected: true});
            $scope.hasSelected = selected.length ?  true : false;
            $scope.selectall = selected.length == $scope.categories.length ? true : false;
        }
    };

    $scope.open = function (size) {
        var addcategoryModal = $modal.open({
            templateUrl: baseurl + 'scripts/views/categoriesModal.html',
            controller: 'AddCategoryModalCtrl',
            size: size
        });
    };

    $scope.edit = function (id) {
        var editlocationModal = $modal.open({
            templateUrl: baseurl + 'scripts/views/categoriesModal.html',
            controller: 'EditCategoryModalCtrl',
            resolve: {
                id: function() {
                    return angular.copy(id);
                }
            }
        });
    };

    $scope.delete = function (id) {
        var categoriesToDelete = _.find($scope.categories, function(categoryitem){ return categoryitem.id == id; });
        
        if (confirm("Are you sure you want to delete " + categoriesToDelete.name + " (id:" + categoriesToDelete.id + ")?") == true) {
            CategoriesService.deleteCategory(id).then(function(data) {
                $window.location.reload();
            });
        } else {
        }
    };

    $scope.deleteAll = function () {
        var selected = _.where($scope.categories, {selected: true});

        var selectedtext = '\n';
        _.each( selected, 
            function(categoriesitemselected){ 
                selectedtext += '   -' + categoriesitemselected.name + ' (id:' + categoriesitemselected.id + ')\n'; 
            }
        );

        if (confirm('Are you sure you want to delete:' + selectedtext) == true){
            CategoriesService.deleteCategory(selected).then(function(data) {
                $window.location.reload();
            });
        }
    };

});

ratewhatyouateApp.controller('AddCategoryModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'CategoriesService', '$window', function ($scope, $modalInstance, $fileUploader, CategoriesService, $window) {

    $scope.new = true;
    $scope.category= {};
    $scope.saveButton = 'Add Category';
    $scope.disableform = false;

    $scope.ok = function(category) {

        if( CategoriesService.isCategory(category) ){
            $scope.saveButton = 'Adding...';
            // $scope.disableform = true;

            CategoriesService.addCategory(category).then(function(data) {
                $modalInstance.close();
                $window.location.reload();
            });
        }
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        return CategoriesService.check(form, $scope['category'][form]) ? '' : 'has-error';
    };

}]);

ratewhatyouateApp.controller('EditCategoryModalCtrl', [ '$scope', '$modalInstance', '$fileUploader', 'CategoriesService', '$window', 'id', function ($scope, $modalInstance, $fileUploader, CategoriesService, $window, id) {

    this.$inject = ['id'];
    $scope.new = false;
    CategoriesService.getCategoryItem(id).then(function(data) {
        $scope.category = data[0];
        var starttime = new Date($scope.category.starttime);
        var endtime = new Date($scope.category.endtime);
        $scope.category.starttime = pad(starttime.getHours(),2) + ':' + pad(starttime.getMinutes(), 2);
        $scope.category.endtime = pad(endtime.getHours(),2) + ':' + pad(endtime.getMinutes(), 2);
    });
    $scope.saveButton = 'Save Category';
    $scope.disableform = false;

    $scope.ok = function(category) {

        if( CategoriesService.isCategory(category) ){
            $scope.saveButton = 'Saving...';
            $scope.disableform = true;

            CategoriesService.editCategory(id, category).then(function(data) {
                $modalInstance.close();
                $window.location.reload();
            });
        }
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };

    $scope.isdisabled = function () {
        return $scope.disableform;
    };

    $scope.hasError = function (form) {
        try { 
            return CategoriesService.check(form, $scope['category'][form]) ? '' : 'has-error';
        } catch(e) {

        }
    };

    function pad(n){return n<10 ? '0'+n : n}

}]);