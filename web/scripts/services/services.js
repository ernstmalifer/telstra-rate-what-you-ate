ratewhatyouateApp.service('MenuService', ['Restangular', '$http', '_', function(Restangular, $http, _) {
    this.getMenu = function() {
    	return Restangular.all('api/menu').getList();
    }
    this.getMenuItem = function(id) {
        return Restangular.one('api/menu/' + id).get();
    }
    this.addMenu = function(menu) {
    	return Restangular.all('api/menu').post(menu);
    }
    this.editMenu = function(id, menu) {
        return Restangular.all('api/menu/' + id).post(menu);
    }
    this.deleteMenu = function(ids) {
        if(ids instanceof Array) {
            var querystring = [];
            _.each( ids, 
                function(menuitemselected){ 
                    querystring.push(menuitemselected.id); 
                }
            );

            return Restangular.all('api/menu?').remove({"id[]":querystring});
        } else {
            return Restangular.all('api/menu/' + id).remove();
        }
    }
    this.isMenu = function(menu) {
    	return( this.check('name', menu.name) && this.check('price', menu.price) && this.check('description', menu.description) );
    }
    this.check = function(form, value) {
    	switch(form){
    		case 'name':
    			return value ? true : false;
    			break;
    		case 'price':
    			if( value &&  !isNaN(value)) { return true; } else { return false };
    			break;
    		case 'description':
    			return value ? true : false;
    			break;
    	}
    }
}]);

ratewhatyouateApp.service('LocationsService', ['Restangular', '$http', '_', function(Restangular, $http, _) {
    this.getLocations = function() {
        return Restangular.all('api/locations').getList();
    }
    this.getLocationItem = function(id) {
        return Restangular.one('api/locations/' + id).get();
    }
    this.addLocation = function(location) {
        return Restangular.all('api/locations').post(location);
    }
    this.editLocation = function(id, menu) {
        return Restangular.all('api/locations/' + id).post(menu);
    }
    this.deleteLocation = function(ids) {
        if(ids instanceof Array) {
            var querystring = [];
            _.each( ids, 
                function(locationsitemselected){ 
                    querystring.push(locationsitemselected.id); 
                }
            );

            return Restangular.all('api/locations?').remove({"id[]":querystring});
        } else {
            return Restangular.all('api/locations/' + id).remove();
        }
    }
    this.isLocation = function(location) {
        return( this.check('name', location.name) && this.check('description', location.description), this.check('accesskey', location.accesskey) );
    }
    this.check = function(form, value) {
        switch(form){
            case 'name':
                return value ? true : false;
                break;
            case 'description':
                return value ? true : false;
                break;
            case 'accesskey':
                return value ? true : false;
                break;
        }
    }
}]);

ratewhatyouateApp.service('CategoriesService', ['Restangular', '$http', '_', function(Restangular, $http, _) {
    this.getCategories = function() {
        return Restangular.all('api/categories').getList();
    }
    this.getCategoryItem = function(id) {
        return Restangular.one('api/categories/' + id).get();
    }
    this.addCategory = function(category) {
        return Restangular.all('api/categories').post(category);
    }
    this.editCategory = function(id, menu) {
        return Restangular.all('api/categories/' + id).post(menu);
    }
    this.deleteCategory = function(ids) {
        if(ids instanceof Array) {
            var querystring = [];
            _.each( ids, 
                function(categoriesitemselected){ 
                    querystring.push(categoriesitemselected.id); 
                }
            );

            return Restangular.all('api/categories?').remove({"id[]":querystring});
        } else {
            return Restangular.all('api/categories/' + ids).remove();
        }
    }
    this.isCategory = function(category) {
        return( this.check('name', category.name) && this.check('starttime', category.starttime) && this.check('endtime', category.endtime) );
    }
    this.check = function(form, value) {
        switch(form){
            case 'name':
                return value ? true : false;
                break;
            case 'starttime':
                return value ? true : false;
                break;
            case 'endtime':
                return value ? true : false;
                break;
        }
    }
}]);

ratewhatyouateApp.factory('_', function() {
        return window._; // assumes underscore has already been loaded on the page
});