<?php

namespace Telstra\RatewhatyouateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClientController extends Controller
{
    public function indexAction()
    {
        return $this->render('TelstraRatewhatyouateBundle:Client:index.html.twig');
    }
}
