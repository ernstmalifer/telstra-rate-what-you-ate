<?php

namespace Telstra\RatewhatyouateBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Post,
    FOS\RestBundle\Controller\Annotations\Delete;

use JMS\SerializerBundle\Serializer;
use JMS\Serializer\SerializationContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Telstra\RatewhatyouateBundle\Entity\Location;

class LocationsApiController extends FOSRestController
{
    /**
     * GET Route annotation.
     * @Get("/api/locations")
     */
    public function getLocationsAction()
    {

        $em = $this->getDoctrine()->getManager();

        // Returns object as compared to array with a single element
        // $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
        //                     ->findAll();

        $locations = $em->getRepository('TelstraRatewhatyouateBundle:Location')
                    ->createQueryBuilder('l')
                    ->where('l.isarchived != true')
                    ->getQuery()
                    ->getResult();

        $serializer = $this->container->get('serializer');
        $serializeLocations = $serializer->serialize($locations, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeLocations . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

    /**
     * GET Route annotation.
     * @Post("/api/locations")
     */
    public function postLocationAction(Request $request)
    {
        $postlocation = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $location = new Location;
        $location->setName($postlocation['name']);
        $location->setDescription($postlocation['description']);
        $location->setAccesskey($postlocation['accesskey']);
        
        $em->persist($location);
        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "id": "' . $location->getId() . '"} }' );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/locations/{id}")
     */
    public function getLocationItemAction($id)
    {
        $response = new Response();

        $em = $this->getDoctrine()->getManager();

        $location = $em->getRepository('TelstraRatewhatyouateBundle:Location')
                    ->createQueryBuilder('l')
                    ->where('l.id = ' . $id)
                    ->andWhere('l.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$location) {
            $response->setContent( '{"success":false, "data": { "message": "No location with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $serializer = $this->container->get('serializer');
            $serializeLocation = $serializer->serialize($location, 'json');

            $response->setContent( '{"success":true, "data":' . $serializeLocation . '}' );
            $response->headers->set('Content-Type', 'application/json');
            
            return $response;
        }
    }

    /**
     * POST Route annotation.
     * @Post("/api/locations/{id}")
     */
    public function postLocationItemAction($id, Request $request)
    {
        $response = new Response();

        $postlocation = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository('TelstraRatewhatyouateBundle:Location')
                    ->createQueryBuilder('l')
                    ->where('l.id = ' . $id)
                    ->andWhere('l.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$location) {
            $response->setContent( '{"success":false, "data": { "message": "No location with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $location[0]->setName($postlocation['name']);
            $location[0]->setDescription($postlocation['description']);
            $location[0]->setAccesskey($postlocation['accesskey']);

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $location[0]->getId() . '"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/locations")
     */
    public function deleteSomeAllLocationsAction(Request $request)
    {

        $getlocations = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $locations = $em->getRepository('TelstraRatewhatyouateBundle:Location');
        foreach ($locations->findById($getlocations) as $locationitem) {
            $locationitem->setIsarchived(1);
        }

        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "message": "Location items are successfully deleted"} }' );
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/locations/{id}")
     */
    public function deleteLocationAction($id)
    {

        $response = new Response();
        
        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository('TelstraRatewhatyouateBundle:Location')
                    ->createQueryBuilder('l')
                    ->where('l.id = ' . $id)
                    ->andWhere('l.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$location) {
            $response->setContent( '{"success":false, "data": { "message": "No location with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $location[0]->setIsarchived(1);

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $location[0]->getId() . '", "message": "Location item is successfully deleted"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

    }

}