<?php

namespace Telstra\RatewhatyouateBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Post,
    FOS\RestBundle\Controller\Annotations\Delete;

use JMS\SerializerBundle\Serializer;
use JMS\Serializer\SerializationContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Telstra\RatewhatyouateBundle\Entity\Menu;

class MenuApiController extends FOSRestController
{
    /**
     * GET Route annotation.
     * @Get("/api/menu")
     */
    public function getMenuAction()
    {

        $em = $this->getDoctrine()->getManager();

        // Returns object as compared to array with a single element
        // $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
        //                     ->findAll();

        $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
                    ->createQueryBuilder('m')
                    ->where('m.isarchived != true')
                    ->getQuery()
                    ->getResult();

        $serializer = $this->container->get('serializer');
        $serializeMenu = $serializer->serialize($menu, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeMenu . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

    /**
     * GET Route annotation.
     * @Post("/api/menu")
     */
    public function postMenuAction(Request $request)
    {
        $postmenu = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $menu = new Menu;
        $menu->setName($postmenu['name']);
        $menu->setDescription($postmenu['description']);
        $menu->setPrice($postmenu['price']);
        
        $em->persist($menu);
        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "id": "' . $menu->getId() . '"} }' );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/menu/{id}")
     */
    public function getMenuItemAction($id)
    {
        $response = new Response();

        $em = $this->getDoctrine()->getManager();

        $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
                    ->createQueryBuilder('m')
                    ->where('m.id = ' . $id)
                    ->andWhere('m.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$menu) {
            $response->setContent( '{"success":false, "data": { "message": "No menu with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $serializer = $this->container->get('serializer');
            $serializeMenu = $serializer->serialize($menu, 'json');

            $response->setContent( '{"success":true, "data":' . $serializeMenu . '}' );
            $response->headers->set('Content-Type', 'application/json');
            
            return $response;
        }
    }

    /**
     * POST Route annotation.
     * @Post("/api/menu/{id}")
     */
    public function postMenuItemAction($id, Request $request)
    {
        $response = new Response();

        $postmenu = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
                    ->createQueryBuilder('m')
                    ->where('m.id = ' . $id)
                    ->andWhere('m.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$menu) {
            $response->setContent( '{"success":false, "data": { "message": "No menu with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $menu[0]->setName($postmenu['name']);
            $menu[0]->setDescription($postmenu['description']);
            $menu[0]->setPrice($postmenu['price']);

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $menu[0]->getId() . '"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/menu")
     */
    public function deleteSomeAllMenuAction(Request $request)
    {

        $getmenu = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu');
        foreach ($menu->findById($getmenu) as $menuitem) {
            $menuitem->setIsarchived(1);
        }

        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "message": "Menu items are successfully deleted"} }' );
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/menu/{id}")
     */
    public function deleteMenuAction($id)
    {

        $response = new Response();
        
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
                    ->createQueryBuilder('m')
                    ->where('m.id = ' . $id)
                    ->andWhere('m.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$menu) {
            $response->setContent( '{"success":false, "data": { "message": "No menu with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            // print_r($menu[0]);
            $menu[0]->setIsarchived(1);

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $menu[0]->getId() . '", "message": "Menu item is successfully deleted"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

    }

    /**
     * POST Route annotation.
     * @Post("/api/menu/image/{id}")
     */
    public function postMenuImageAction($id)
    {
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

        $uploadDir = $this->container->getParameter('menuupload_dir');
        move_uploaded_file(
            $_FILES['file']['tmp_name'], 
            $uploadDir . '/' . $id . '.jpg'
        );

        $response = new Response();
        $response->setContent( '{"success":true, "data": "' . $id . '.jpg" }' );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/schedules")
     */
    public function getScheduleAction()
    {

        $em = $this->getDoctrine()->getManager();

        $schedules = $em->getRepository('TelstraRatewhatyouateBundle:Schedule')
                            ->findAll();

        $serializer = $this->container->get('serializer');
        $serializeSchedules = $serializer->serialize($schedules, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeSchedules . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/locations")
     */
    public function getLocationsAction()
    {

        $em = $this->getDoctrine()->getManager();

        $locations = $em->getRepository('TelstraRatewhatyouateBundle:Location')
                            ->findAll();

        $serializer = $this->container->get('serializer');
        $serializeLocations = $serializer->serialize($locations, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeLocations . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/categories")
     */
    public function getCategoriesAction()
    {

        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('TelstraRatewhatyouateBundle:Category')
                            ->findAll();

        $serializer = $this->container->get('serializer');
        $serializeCategories = $serializer->serialize($categories, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeCategories . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

}