<?php

namespace Telstra\RatewhatyouateBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Post,
    FOS\RestBundle\Controller\Annotations\Delete;

use JMS\SerializerBundle\Serializer;
use JMS\Serializer\SerializationContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Telstra\RatewhatyouateBundle\Entity\Category;

class CategoriesApiController extends FOSRestController
{
    /**
     * GET Route annotation.
     * @Get("/api/categories")
     */
    public function getCategoriesAction()
    {

        $em = $this->getDoctrine()->getManager();

        // Returns object as compared to array with a single element
        // $menu = $em->getRepository('TelstraRatewhatyouateBundle:Menu')
        //                     ->findAll();

        $categories = $em->getRepository('TelstraRatewhatyouateBundle:Category')
                    ->createQueryBuilder('c')
                    ->where('c.isarchived != true')
                    ->getQuery()
                    ->getArrayResult();



        $serializer = $this->container->get('serializer');
        $serializeCategories = $serializer->serialize($categories, 'json');

        $response = new Response();
        $response->setContent( '{"success":true, "data":' . $serializeCategories . '}' );
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
    }

    /**
     * GET Route annotation.
     * @Post("/api/categories")
     */
    public function postCategoriesAction(Request $request)
    {
        $postcategory = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $category = new Category;
        $starttime = new \DateTime;
        $endtime = new \DateTime;

        $category->setName($postcategory['name']);
        $category->setStarttime( $starttime->setTime(substr($postcategory['starttime'], 0, 2), substr($postcategory['starttime'], 3, 2) ));
        $category->setEndtime( $endtime->setTime(substr($postcategory['endtime'], 0, 2), substr($postcategory['endtime'], 3, 2) ));
        
        $em->persist($category);
        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "id": "' . $category->getId() . '"} }' );
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * GET Route annotation.
     * @Get("/api/categories/{id}")
     */
    public function getCategoryItemAction($id)
    {
        $response = new Response();

        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('TelstraRatewhatyouateBundle:Category')
                    ->createQueryBuilder('c')
                    ->where('c.id = ' . $id)
                    ->andWhere('c.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$category) {
            $response->setContent( '{"success":false, "data": { "message": "No location with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $serializer = $this->container->get('serializer');
            $serializeCategory = $serializer->serialize($category, 'json');

            $response->setContent( '{"success":true, "data":' . $serializeCategory . '}' );
            $response->headers->set('Content-Type', 'application/json');
            
            return $response;
        }
    }

    /**
     * POST Route annotation.
     * @Post("/api/categories/{id}")
     */
    public function postCategoryItemAction($id, Request $request)
    {
        $response = new Response();

        $postcategory = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('TelstraRatewhatyouateBundle:Category')
                    ->createQueryBuilder('c')
                    ->where('c.id = ' . $id)
                    ->andWhere('c.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$category) {
            $response->setContent( '{"success":false, "data": { "message": "No category with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $starttime = new \DateTime;
            $endtime = new \DateTime;

            $category[0]->setName($postcategory['name']);
            $category[0]->setStarttime( $starttime->setTime(substr($postcategory['starttime'], 0, 2), substr($postcategory['starttime'], 3, 2) ));
            $category[0]->setEndtime( $endtime->setTime(substr($postcategory['endtime'], 0, 2), substr($postcategory['endtime'], 3, 2) ));

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $category[0]->getId() . '"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/categories")
     */
    public function deleteSomeAllCategoriesAction(Request $request)
    {

        $getcategories = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('TelstraRatewhatyouateBundle:Category');
        foreach ($category->findById($getcategories) as $categoryitem) {
            $categoryitem->setIsarchived(1);
        }

        $em->flush();

        $response = new Response();
        $response->setContent( '{"success":true, "data": { "message": "Category items are successfully deleted"} }' );
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * DELETE Route annotation.
     * @Delete("/api/categories/{id}")
     */
    public function deleteCategoryAction($id)
    {

        $response = new Response();
        
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('TelstraRatewhatyouateBundle:Category')
                    ->createQueryBuilder('c')
                    ->where('c.id = ' . $id)
                    ->andWhere('c.isarchived != true')
                    ->getQuery()
                    ->getResult();

        if (!$category) {
            $response->setContent( '{"success":false, "data": { "message": "No category with id ' . $id . '"} }' );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $category[0]->setIsarchived(1);

            $em->flush();

            $response->setContent( '{"success":true, "data": { "id": "' . $category[0]->getId() . '", "message": "Category item is successfully deleted"} }' );
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

    }

}