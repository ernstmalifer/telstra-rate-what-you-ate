<?php

namespace Telstra\RatewhatyouateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Schedule
 *
 * @ORM\Table(name="schedule")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Schedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     * @Expose
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     * @Expose
     */
    private $weight;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Schedule
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Schedule
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="schedule")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", nullable=true)
     * @Expose
     */
    private $menu;

    /**
     * @ORM\ManyToOne(targetEntity="Location", inversedBy="schedule")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=true)
     * @Expose
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="schedule")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     * @Expose
     */
    private $category;
    

    /**
     * Set menu
     *
     * @param \Telstra\RatewhatyouateBundle\Entity\Menu $menu
     * @return Schedule
     */
    public function setMenu(\Telstra\RatewhatyouateBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return \Telstra\RatewhatyouateBundle\Entity\Menu 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set location
     *
     * @param \Telstra\RatewhatyouateBundle\Entity\Location $location
     * @return Schedule
     */
    public function setLocation(\Telstra\RatewhatyouateBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \Telstra\RatewhatyouateBundle\Entity\Location 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set category
     *
     * @param \Telstra\RatewhatyouateBundle\Entity\Category $category
     * @return Schedule
     */
    public function setCategory(\Telstra\RatewhatyouateBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Telstra\RatewhatyouateBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
