module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
      },
      build: {
        src: [
          'web/bower_components/angular/angular.js',
          'web/bower_components/angular-resource/angular-resource.min.js',
          'web/bower_components/angular-route/angular-route.min.js',
          'web/bower_components/lodash/dist/lodash.underscore.min.js',
          'web/bower_components/restangular/dist/restangular.min.js',
          'web/bower_components/angular-ui-bootstrap/ui-bootstrap-tpls-0.11.0.min.js',
          'web/bower_components/angular-file-upload/angular-file-upload.min.js'
        ],
        dest: 'web/build/libraries.min.js'
      }
    },
    ngmin: {
      controllers: {
        src: [
          'web/scripts/app.js',
          'web/scripts/controllers/homecontroller.js',
          'web/scripts/controllers/menucontroller.js',
          'web/scripts/controllers/locationscontroller.js',
          'web/scripts/controllers/categoriescontroller.js',
          'web/scripts/controllers/navbarcontroller.js',
          'web/scripts/directives/directives.js',
          'web/scripts/services/services.js'
        ],
        dest: 'web/build/app.js'
      },
    },
    watch: {
      scripts: {
        files: ['**/*.js'],
        tasks: ['default'],
        options: {
          spawn: false,
          livereload: {
            port: 6969
          }
        },
      },
    },
  });

  // Load the plugin that provides the "ngmin" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-ngmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'ngmin']);

  // 
  // grunt.registerTask('watch', ['watch']);

};
