<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140521060936 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
			CREATE TABLE IF NOT EXISTS `category` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  `starttime` time NOT NULL,
			  `endtime` time NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

			-- --------------------------------------------------------

			--
			-- Table structure for table `location`
			--

			CREATE TABLE IF NOT EXISTS `location` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

			-- --------------------------------------------------------

			--
			-- Table structure for table `menu`
			--

			CREATE TABLE IF NOT EXISTS `menu` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
			  `price` double NOT NULL,
			  `created` datetime NOT NULL,
  			  `isarchived` tinyint(1) DEFAULT \'0\',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

			-- --------------------------------------------------------

			--
			-- Table structure for table `migration_versions`
			--

			CREATE TABLE IF NOT EXISTS `migration_versions` (
			  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
			  PRIMARY KEY (`version`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

			-- --------------------------------------------------------

			--
			-- Table structure for table `schedule`
			--

			CREATE TABLE IF NOT EXISTS `schedule` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `menu_id` int(11) DEFAULT NULL,
			  `location_id` int(11) DEFAULT NULL,
			  `date` date NOT NULL,
			  `weight` int(11) NOT NULL,
			  `category_id` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  KEY `IDX_5A3811FBCCD7E912` (`menu_id`),
			  KEY `IDX_5A3811FB64D218E` (`location_id`),
			  KEY `IDX_5A3811FB12469DE2` (`category_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

			--
			-- Constraints for dumped tables
			--

			--
			-- Constraints for table `schedule`
			--
			ALTER TABLE `schedule`
			  ADD CONSTRAINT `FK_5A3811FB12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
			  ADD CONSTRAINT `FK_5A3811FB64D218E` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`),
			  ADD CONSTRAINT `FK_5A3811FBCCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`);
			');

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
